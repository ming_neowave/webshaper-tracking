'use strict'
const mongoose = require('mongoose');

let ReportSchema = new mongoose.Schema({
	domain:String,
	total_unique_visitor: Number,
	page_view: Number,
	for_date: Date
},{ timestamps : true});

module.exports = mongoose.model('Report', ReportSchema);