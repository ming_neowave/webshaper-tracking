'use strict'

const mongoose = require('mongoose');

let TrackingInfoSchema = new mongoose.Schema({},{ timestamps : true});

module.exports = mongoose.model('TrackingInfo', TrackingInfoSchema);