'use strict'
const express = require('express');
const app = express();
const cookieParser = require('cookie-parser')
const mongoose = require('mongoose');
const apiRouter = require('./api.js');

mongoose.connect('mongodb://localhost/tracker');
// Use native promises
mongoose.Promise = global.Promise;

const WsTracker = require('./WsTracker.js');

let tracker = require('pixel-tracker');

app.use(express.static('public'));

app.use(cookieParser());

let t = new WsTracker;

tracker.use( t.execute() )
.configure({ disable_cookies : false })

app.get('/product.gif',(req, res, next) => {
	return tracker.middleware(req,res,next);
});

app.use('/api',apiRouter);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})

