'use strict'
const moment = require('moment');

let TrackingInfo = require('./models/TrackInfo.js');
let Report = require('./models/Report.js');

class WsTracker {

	constructor(){
		console.log(this);
		console.log('constructed')
	}

	execute (){
		let vm = this;
		return (err,result) => {
			let {_tracker} = result.cookies;

			result.domain = vm.santizeHost(result.domain);
			
			vm.saveInfo(result);

			vm.saveUniqueVistor(_tracker,result.domain);
		}
	}

	santizeHost(host){
		let parts = host.split('.');

		if(parts[0] == 'www'){
			parts.shift();
			host = parts.join('.');
		}

		let wsTemp = /\.ws[3-8]\.webshaper\.com\.my/g;

		host = host.replace(wsTemp,'');

		return host;
	}

	saveInfo(info){
		let startDay = moment().startOf('day');
		let {_tracker} = info.cookies;
		let {domain,path} = info;
		let vm = this;
		TrackingInfo.findOne({ 'cookies._tracker' : _tracker, domain: domain,path:path, createdAt: {$gte: startDay } })
		.then( track => {
			if(!track){

				let track = new TrackingInfo(info,false);
				track.save();

				return Report.findOne({ domain: domain, for_date: startDay.format('Y-MM-DD')});
			}

			return false;
		})
		.then( report => {

			if(report == false){
				return null;
			}

			return report;
		})
		.then( report => {
			if(!report){
				return;
			}
			report.page_view += 1;
			report.save();
		})
	}

	saveUniqueVistor(tracker,domain){
		let startDay = moment().startOf('day');

		TrackingInfo.findOne({ 'cookies._tracker' : tracker, domain: domain, createdAt: {$gte: startDay } })
		.then( track => {
			if(!track){ //not exists,is unique vistor
				return Report.findOne({ domain: domain, for_date: startDay.format('Y-MM-DD')})
			}
			return false;
		})
		.then( report => {
			if(report == false){
				// false means not unique visitor
				return null;
			}

			if(report == null){
				report = new Report({
					domain:domain,
					total_unique_visitor : 0,
					page_view: 1,
					for_date : startDay.format('Y-MM-DD')
				});
			}
			
			report.total_unique_visitor += 1;

			return report.save()
		});
	}
}

module.exports = WsTracker