'use strict';
const express = require('express')
const router = express.Router()
const moment = require('moment');

let TrackingInfo = require('./models/TrackInfo.js');
let Report = require('./models/Report.js');

const allowedAddress = ['::1','175.136.217.249','210.5.43.246','::ffff:210.5.43.246'];
const allowedOrigin = ['localhost:8080','v1.webshaper.com.my'];

// router.use( (req,res,next) => {

// 	if( allowedOrigin.indexOf(req.headers.host) > -1 )
// 	{
// 		res.header("Access-Control-Allow-Origin", req.headers.host);
// 		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
// 	}

// 	next();
	
// });

router.use( (req,res,next) => {
	if(req.headers['x-forwarded-for'])
	{
		let addr = req.headers['x-forwarded-for'];
		if(allowedAddress.indexOf(addr) > -1){
			next();
		}else
		{
			res.send('unauthorized - '+addr);
		}
	}else
	{
		_getRemoteAddress(req, remoteAddress => {
			if(allowedAddress.indexOf(remoteAddress) > -1){
				next();
			}else{
				res.send('unauthorized - '+remoteAddress);
			}
		});
	}
	
});



router.get('/today-top-5-high-traffic', (req,res) => {
	let startDay = moment().utc().startOf('day');


	Report.find({ for_date: { $gte: startDay.toDate() }, domain : { $ne : 'undefined'} } )
	.sort({ page_view: -1})
	.limit(5)
	.then( reports => {
		res.json(reports);
	})
})

router.get('/today-top-5-unique-visitors', (req,res) => {
	let startDay = moment().utc().startOf('day');


	Report.find({ for_date: { $gte: startDay.toDate() }, domain : { $ne : 'undefined'} } )
	.sort({ total_unique_visitor: -1})
	.limit(5)
	.then( reports => {
		res.json(reports);
	})
})

router.get('/weekly-top-5-high-traffic', (req,res) => {
	let sixDaysAgo = moment().utc().subtract(6,'days').startOf('day');
	let tomorrow = moment().utc().add(1,'days').startOf('day');

	let from = sixDaysAgo.format('DD-MM-YY');
	let to = moment().utc().format('DD-MM-YY');

	Report.aggregate(
	{
		$match : { for_date : { $gte: sixDaysAgo.toDate(), $lte: tomorrow.toDate() }, domain : { $ne : 'undefined'} }	
	},
	{
		$group: { _id: "$domain", total_page_view:{ $sum: '$page_view' } }
	},
	{
		$sort: { total_page_view: -1 }
	},
	{
		$limit : 5
	})
	.then( reports => {
		res.json({
			from,
			to,
			reports
		});
	})
});

router.get('/weekly-top-5-unique-visitors', (req,res) => {
	let sixDaysAgo = moment().utc().subtract(6,'days').startOf('day');
	let tomorrow = moment().utc().add(1,'days').startOf('day');

	let from = sixDaysAgo.format('DD-MM-YY');
	let to = moment().utc().format('DD-MM-YY');

	Report.aggregate(
	{
		$match : { for_date : { $gte: sixDaysAgo.toDate(), $lte: tomorrow.toDate() }, domain : { $ne : 'undefined'} }	
	},
	{
		$group: { _id: "$domain", total_unique_visitors:{ $sum: '$total_unique_visitor' } }
	},
	{
		$sort: { total_unique_visitors: -1 }
	},
	{
		$limit : 5
	})
	.then( reports => {
		res.json({
			from,
			to,
			reports
		});
	})
});

router.get('/stats/:domain', (req,res) => {
	Promise.all([
		_getTop10PageViewed(req.params.domain),
		_getPass7DaysReports(req.params.domain),
		_getWeeklyPageViewTime(req.params.domain)
	])
	.then( results => {
		let infos = results[0];
		let reports = results[1];
		let visitTime = results[2];

		return res.json({
			domain: req.params.domain,
			top_10_page_viewed: infos,
			reports: reports,
			visitTime: visitTime
		});
	})
})

function _getRemoteAddress (req,callback) {
  var rc = req.connection
  if (req.socket && req.socket.remoteAddress) 
    callback(req.socket.remoteAddress)
  else if (rc) 
    if (rc.remoteAddress)
      callback(rc.remoteAddress)
    else if (rc.socket && rc.socket.remoteAddress) 
      callback(rc.socket.remoteAddress)
  else {
    console.log('ERR: no remoteAddress')
    callback(null)
  }
}

function _getTop10PageViewed(domain)
{
	let sixDaysAgo = moment().utc().subtract(6,'days').startOf('day');

	let tomorrow = moment().utc().add(1,'days').startOf('day');

	return TrackingInfo.aggregate(
	{
	    $match: { 
	        createdAt:{ $lte: tomorrow.toDate(), $gte: sixDaysAgo.toDate() } ,
	        domain : domain
	    }
	},
	{
	    
	    $group: { _id: "$path", count:{ $sum:1}, lastVisited : { $first: '$createdAt' } }
	},
	{
	    $sort : { count : -1 }
	},
	{
		$limit : 10
	})
	.then( infos => infos.map( info => {
			info.lastVisited = moment(info.lastVisited).format('H:mm DD-MM-YY');
			return info
		})
	)
}

function _getWeeklyPageViewTime(domain)
{
	let sixDaysAgo = moment().utc().subtract(6,'days').startOf('day');

	let tomorrow = moment().utc().add(1,'days').startOf('day');

	return TrackingInfo.aggregate(
	{
	    $match: { 
	        createdAt:{ $lte: tomorrow.toDate(), $gte: sixDaysAgo.toDate() } ,
	        domain : domain
	    }
	},
	{
	    
	    $group: { _id: {$hour : '$createdAt'} , count:{ $sum:1} }
	},
	{
	    $sort : { count : -1 }
	},
	{
		$limit : 10
	})
	.then( infos => infos.map( info => {
			info._id = info._id + 8;

			if(info._id > 24){
				info._id -= 24;
			}

			return info
		})
	)
	.then( infos => infos.sort( (a,b) => a._id-b._id ));
}

function _getPass7DaysReports(domain)
{
	let sixDaysAgo = moment().utc().subtract(6,'days').startOf('day');

	let tomorrow = moment().utc().add(1,'days').startOf('day');

	return Report.find({ createdAt:{ $lte: tomorrow.toDate(), $gte: sixDaysAgo.toDate() }, domain : domain })
}
module.exports = router;